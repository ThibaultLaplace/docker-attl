import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

public class BulletinMeteo extends Bulletin{
	private String zone_geo;

	//Accesseurs
	public String getZone() {
		return this.zone_geo;
	}
	
	//Modificateurs
	public void setZone(String z) {
		this.zone_geo=z;
	}
	
	//Constructeurs
	public BulletinMeteo() {
		super();
		String[] geoZones = {"Annecy", "Paris", "Lyon", "Chambery"};
		int randomGeoZonesNum = ThreadLocalRandom.current().nextInt(0,geoZones.length);
		this.setZone(geoZones[randomGeoZonesNum]);
	}
	
	//Methodes
	public String toString() {
		return super.toString() + // appel de la methode de la super-classe
		" (" + this.getZone() + ") "; // on specialise
		}


	public static void main(String[] args) {
	}
}
