import java.util.ArrayList;
import java.util.Iterator;

public class EnsembleDeBulletinsMeteo extends ArrayList<BulletinMeteo> {
	// On modifie cette methode TP 4 - cette methode redevient une methode d'instance
	public void afficherBulletins() {
		System.out.println("===== Affichage des bulletins =====\n");
		for (Bulletin bulletin : this) { // this est UNE collection d'objets
			System.out.println(bulletin.toString());
		}
	}
	
	public EnsembleDeBulletinsMeteo rechercherBulletins(String zoneG) {
		EnsembleDeBulletinsMeteo bulletins = new EnsembleDeBulletinsMeteo();
		for (BulletinMeteo bulletin : this) {
			if (bulletin.getZone() == zoneG)
				// 	Si la zoneGeo du bulletin correspond a celle recherchee on ajoute le bulletin
				bulletins.add(bulletin);
		}
		return bulletins;
	}
	// Elle permet de retourner le premier bulletin trouve dans l'historique dont la zone geo correspond a celle passee en parametre.
	public BulletinMeteo rechercherBulletin(String zoneG) {
		BulletinMeteo trouve = null;
		int i = 0;
		while (trouve == null && i < this.size()) {
			trouve = this.get(i);
			if (trouve.getZone() != zoneG)
				trouve = null;
			i++;
		}
		return trouve;
	}
	public void supprimerTousLesBulletins(String zone) {
		Iterator<BulletinMeteo> iter = this.iterator();
		while (iter.hasNext()) {
			BulletinMeteo bulletin = iter.next();
			if (bulletin.getZone() == zone)
				iter.remove();
		}
	}

}
	
	

