import java.util.concurrent.ThreadLocalRandom;

public class BulletinAvalanche extends BulletinMeteo{
	private int hauteurNeigeFraiche;
	private int niveauRisque =3;
	
	//Setter
	public void setHauteurNeigeFraiche(int h) {
		this.hauteurNeigeFraiche=h;
	}
	public void setNiveauRisque(int r) {
		this.niveauRisque=r;
	}
	
	//Getter
	public int getHauteurNeigeFraiche() {
		return this.hauteurNeigeFraiche;
	}
	public int getNiveauRisque() {
		return this.niveauRisque;
	}
	//Constructeur
	public BulletinAvalanche() {
		super();
		int[] hauteursNeige = {0, 10, 20, 30, 50};
		int[] niveauxRisque = {0, 1, 2, 3, 4, 5};
		int randomHauteurNeigeNum, randomNiveauRisqueNum;
		randomHauteurNeigeNum = ThreadLocalRandom.current().nextInt(0,hauteursNeige.length);
		randomNiveauRisqueNum = ThreadLocalRandom.current().nextInt(0,niveauxRisque.length);
		this.setHauteurNeigeFraiche(hauteursNeige[randomHauteurNeigeNum]);
		this.setNiveauRisque(niveauxRisque[randomNiveauRisqueNum]);
	}
	//Methodes
	public String toString() {
		return super.toString() + // appel de la methode de la super-classe
		 this.getHauteurNeigeFraiche() + " cm de neige fraiche et niveau de risque a " + this.getNiveauRisque(); // on specialise
		}

}
