import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

public class Bulletin {
	private Date date_avis;
	private String avis;
	private static final int MAX_BULLETINS = 10; // c'est un attribut de classe
	
	//Accesseurs
	public String getAvis() {
		return this.avis;
	}
	
	//Modificateurs
	public void setAvis(String a) {
		this.avis=a;
	}
	
	//Constructeurs
	public Bulletin() {
		this.date_avis=new Date();
		String[] tempsQuilFait = {"Grand beau temps", "Pluie", "Quelques averses","Brouillard givrant", "Vent fort", "Nuageux"};
		String[] temperatures = {"Doux", "Chaud", "Froid", "De saison"};
		int randomTempsQuilFaitNum, randomTemperaturesNum;
		randomTempsQuilFaitNum = ThreadLocalRandom.current().nextInt(0,tempsQuilFait.length);
		randomTemperaturesNum = ThreadLocalRandom.current().nextInt(0,temperatures.length);
		this.avis = tempsQuilFait[randomTempsQuilFaitNum] + " - " + temperatures[randomTemperaturesNum];
	}
	public Bulletin(String a) {
		this();
		setAvis(a);
	}
	
	//Methodes
	public String toString() {
		return "Bulletin du " + this.date_avis.toString() +	" - Avis : " + this.getAvis();
		}

	public static void main(String[] args) {
		BulletinMeteo bulletin = new BulletinMeteo();
		System.out.println(bulletin.getAvis());
		System.out.println(bulletin.toString());
	}
}
