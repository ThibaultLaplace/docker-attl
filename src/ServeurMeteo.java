import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ThreadLocalRandom;


// Import de notre classe BulletinMeteo


public class ServeurMeteo {
	// On gere desormais l'historique des bulletins meteo:
		// une collection d'objets, instances de la classe BulletinMeteo
		private EnsembleDeBulletinsMeteo bulletinsMeteo = new EnsembleDeBulletinsMeteo(); 
		private int port = 9090;
		private ServerSocket serveurSocket; 
		private static final int MAX_BULLETINS = 10; // c'est un attribut de classe

		// Méthode pour ouvrir une connexion
		public void ouvrirConnexion() throws IOException {
			this.serveurSocket = new ServerSocket(this.getPort());
		}

		// On utilise desormais les sockets pour avoir une application client-serveur
		public void donnerMeteo() throws IOException {
		    System.out.println("Le serveur est prêt à donner la météo. "
		    		+ "Il est en attente de demandes de client(s) sur le port " + this.getPort() + ".\n");
			try {
				int nbRequetesTraitees = 0;
		    	while (true) {
		    		Socket socket = this.serveurSocket.accept();
		    		try {
		    			PrintWriter out =
	                    new PrintWriter(socket.getOutputStream(), true);
		    			String messageDuServeur = this.getBulletin_courant().toString()+"\n";
		    			out.println(messageDuServeur);
		    			nbRequetesTraitees++;
		    			if ( (nbRequetesTraitees % 5) == 0)
		    				ServeurMeteo.afficherBulletins(this.getBulletinsMeteo()); // toutes les 5 requetes traitees on affiche l'historique
		    			 	this.bulletinsMeteo.add(new BulletinMeteo()); // On reactualise les bulletins meteo par un nouveau bulletin
		    		} finally {
		    			socket.close();
		    		}
		    	}
		    }
		    finally {
		    	this.serveurSocket.close();
		    }
		}
		
		// Utilisation des accesseurs, modificateurs !!!!
		// Modifications exigees
		public void afficherBulletinCourant() {
			System.out.println("Bulletin actuel:\t"+
					this.getBulletin_courant().toString() + "\n");		
		}
		

		public static void afficherBulletins(ArrayList<BulletinMeteo> col) {
			System.out.println("===== Historique des bulletins meteo =====\n");
			for (Bulletin bulletin : col) { 
				System.out.println(bulletin.toString()); 
			}
		}

		public BulletinMeteo getBulletin_precedent() {
			int nbBulletins = this.nbBulletins();
			if (nbBulletins >= 2)
				// l'avant dernier se trouve a la place (la taille de la collection - 2)
				return this.bulletinsMeteo.get(nbBulletins-2);  
			else
				return null;
		}

		public BulletinMeteo getBulletin_courant() {
			int nbBulletins = this.nbBulletins();
			if (nbBulletins >= 1)
				// le dernier se trouve a la place (la taille de la collection - 1)
				return this.bulletinsMeteo.get(nbBulletins-1);  
			else
				return null;
		}
		
		public void ajouterBulletin(BulletinMeteo bulletin) {
			if (!this.bulletinsMeteo.contains(bulletin))
				this.bulletinsMeteo.add(bulletin);
		}

		public int nbBulletins() {
			return this.bulletinsMeteo.size();
		}
		
		public ServeurMeteo() {
			this.genererUnHistoriqueAleatoire();
		}

		public void genererUnHistoriqueAleatoire() {
			int randomNum;
			for (int i = 0; i < MAX_BULLETINS; i++) {
				randomNum = ThreadLocalRandom.current().nextInt(1, 3);
				switch (randomNum) {
					case 1:
						this.bulletinsMeteo.add(new BulletinMeteo());
					break;
					case 2:
						this.bulletinsMeteo.add(new BulletinAvalanche());
						break;
				}
			}
		}
		//Getter
		public EnsembleDeBulletinsMeteo getBulletinsMeteo(){
			return this.bulletinsMeteo;
		}
		public int getPort() {
			return port;
		}
		
		//Setter
		public void setBulletinsMeteo(EnsembleDeBulletinsMeteo listbull) {
			this.bulletinsMeteo=listbull;
		}
		public void setPort(int port) {
			if (port >= 8500 && port <= 9500) {// Intervalle de valeur autorisee du port 
				this.port = port;
				System.out.println("Le port a ete modifie. Il faudrait relancer le serveur !\n");
			}
			else
				System.out.println("Valeur non autorisee: le port n'a pas ete modifie.\n");
		}
		
		public void afficherPort() {
			System.out.println("La valeur du port est : " + this.getPort() + "\n");
		}
		
		// Illustration des modifications
		public static void main(String[] args) {
			// On cree un objet...
			// ServeurMeteo obj = new ServeurMeteo();
			ServeurMeteo serveur = new ServeurMeteo();
			//System.out.println(serveur.nbBulletins()+"\n");
			ServeurMeteo.afficherBulletins(serveur.getBulletinsMeteo().rechercherBulletins("Paris"));
			ServeurMeteo.afficherBulletins(serveur.getBulletinsMeteo());
	    	try {
	    		serveur.ouvrirConnexion();
	    		serveur.donnerMeteo();
	    	}
	    	catch (IOException e) {
	    		System.err.println(e);
	    	}
		}
}
